var Pauljdehmer = function () {

    var $window = $(window),
    $htmlBody = $('html, body'),
    $body = $('body'),
    $modal = $('#modal'),
    $pageContainer = $('#page-container'),
    $mainNav = $('nav > ul > li > span'),
    $content = $('#content'),
    $sections,
    $sectionTitles = $('h2'),
    $sectionTitle,
    $closeButtons = $('section > div > div > p:last-child'),
    $ajaxPopupButtons = $('.ajax-popup'),
    $popupCloseButton = $('#close-button'),
    $popupContainer = $('#popup-container'),
    $popupContent = $('#popup-content'),
    previousScrollTop = 0,
	supports3dTransforms = Modernizr.csstransforms3d;
    
    function windowResizeHandler() {	
        isDesktop = true;	
        if($(document).width() > 600) {
            isDesktop = true;
            isMobile = false;
        } else {
            isDesktop = false;
            isMobile = true;
        }
        
        if (isDesktop) {
            $sectionTitles.parent().removeClass("open");
            $sectionTitles.next("div").css("height", "");
        } else if (isMobile) {
	    if ($content.find(".open").length > 0) {
		$content.find(".open").children("div").css("height", $content.find(".open").children("div").children("div").innerHeight()+"px");
	    }
	}
    }
    
    function mainNavClickedHandler() {
        openAndScrollToSection($("#"+$(this).data("page-location")).find("h2"));
    }
    
    function sectionTitlesClickedHandler() {
	if (isMobile) {
	    if (!$(this).parent().hasClass("open")) {
		openAndScrollToSection($(this));
	    } else {
		closeOtherSections("none");
	    }
	}
    }
    
    function closeButtonsClickedHanlder() {
	closeOtherSections("none");
	$htmlBody.stop().animate({scrollTop: $(this).parent().parent().parent().offset().top}, 500);
    }
    
    function openAndScrollToSection($sectionToOpen) {
	$sectionTitle = $sectionToOpen;
	closeOtherSections($sectionTitle.parent().attr("id"));
    $sectionTitle.parent().addClass("open");
	$sectionTitle.next("div").css("height", $sectionTitle.next("div").children("div").innerHeight()+"px");
	$htmlBody.stop().animate({scrollTop: $sectionToOpen.parent().offset().top}, 500);
    }
    
    function closeOtherSections($sectionToNotClose) {
	$sections = $("section").not($sectionToNotClose);
	$sections.removeClass("open");
	$sections.parent().find("div").css("height", "");
    }
    
    function setAjaxPopups() {
	$(this).on("click",loadAjax);
    }
    
    function loadAjax() {
	$popupContent.load($(this).attr("href"), loadCompleteHandler);
	showLoading();
	return false;
    }
    
    function showLoading() {
	previousScrollTop = $window.scrollTop();
	if (!supports3dTransforms) {
		$pageContainer.css("top", -previousScrollTop);
	} else {
		$pageContainer.css("transform", "translate3d(0,-"+previousScrollTop+"px,0)");
	}
	$body.addClass("visible");
	$body.addClass("loading");
    }
    
    function loadCompleteHandler(response, status, xhr) {
	if(status == "error") {
		$body.addClass("load-error");
	} else {
	    $body.addClass("loaded");
	    openPopup();
	}
    }
    
    function openPopup() {
	$body.addClass("open");
    }
    
    function modalClickedHandler() {
	closePopup();
    }
    
    function closePopup() {
	$body.removeClass("open");
	window.setTimeout(removeLoadingAndContent,200);
    }
    
    function removeLoadingAndContent() {
	$body.removeClass("loaded");
	$body.removeClass("loading");
	if (!supports3dTransforms) {
		$pageContainer.css("top", 0);
	} else {
		$pageContainer.css("transform", "translate3d(0,0,0)");
	}	
	$window.scrollTop(previousScrollTop);
	$body.removeClass("load-error");
	$popupContent.html("");
	window.setTimeout(removeVisible,200);
    }
	
	function removeVisible() {
		$body.removeClass("visible");
	}
    
    function popupCloseButtonClickedHandler() {
	closePopup();
    }
    
    function popupContainerClickedHandler() {
	closePopup();
    }
    
    function popupContainerChildrenClickedHandler(eventObject) {
	eventObject.stopPropagation();
    }
    
    function initialize() {
        $(window).on("resize", windowResizeHandler);
        windowResizeHandler();
	$mainNav.on("click", mainNavClickedHandler);
        $sectionTitles.on("click", sectionTitlesClickedHandler);
	$closeButtons.on("click", closeButtonsClickedHanlder);
	$ajaxPopupButtons.each(setAjaxPopups);
	$modal.on("click", modalClickedHandler);
	$popupCloseButton.on("click", popupCloseButtonClickedHandler);
	$popupContainer.on("click", popupContainerClickedHandler);
	$popupContainer.children().on("click", popupContainerChildrenClickedHandler);
    }
    
    initialize();
}

$(function () {
	var pauljdehmer = new Pauljdehmer();
});